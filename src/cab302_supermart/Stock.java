/**Class that represents a collection of items and their quantities.
 * @author Nathan Morrison
 */
package cab302_supermart;

import java.util.HashMap;

public class Stock {

	private HashMap<Item, Integer> stock;
	
	/**
	 * Constructor for stock. A HashMap that matches each item (the key) to their quantity (the value).
	 */
	public Stock() {
		stock = new HashMap<>();
	}

	/**
	 * Sums the cost of all items within the current stock object.
	 * @return Returns the stock cost as a double.
	 */
	public double getCost() {
		double cost = 0;
		for(Item item : stock.keySet()) {
			cost += item.getCost() * (double)stock.get(item);
		}
		return cost;
	}

	/**
	 * Adds an item to the stock object. Also used for updating the number of items in stock.
	 * @param item Item to be added to the stock.
	 * @param item_count Number of that item to be added or subtracted.
	 * @throws StockException
	 */
	public void addItem(Item item, int item_count) throws StockException {
		//Checks to see whether the item has previously been added to the stock
		//and if so how many of that item already exist.
		int oldItemQuantity = stock.containsKey(item) ? stock.get(item) : 0;
		//Changes quantity by required amount.
		int newItemQuantity = item_count + oldItemQuantity;
		
		//Ensures that there is 0 or more of a particular item in stock, as negative stock is not allowed.
		if (newItemQuantity >= 0) {
			stock.put(item, newItemQuantity);
		}
		else {
			throw new StockException("Cannot have less than 0 items of " + item.getName());
		}
		
	}
	
	/**
	 * Returns the entire stock object.
	 * @return Returns the stock object.
	 */
	public HashMap<Item, Integer> getItems() {
		return stock;
	}

	/**
	 * Returns how many there are in stock of a particular item.
	 * @param item Item that is being queried.
	 * @return Returns the quantity of the item being queried.
	 */
	public int getQuantity(Item item) {
		return stock.get(item);
	}

	/**
	 * Used for writing the stock in the format required for manifests and sales logs.
	 * @param file File to be written to.
	 */
	public void export(CSVWriter file) {
		// TODO Auto-generated method stub
		for(Item item : stock.keySet()) {
			String line = new String(item.getName() + "," + stock.get(item));
			file.writeLine(line);
		}
	
	}

}
