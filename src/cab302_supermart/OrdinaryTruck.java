/**
 * Ordinary Truck. Implements Truck without temperature control.
 * @author Joshua Cao
 */

package cab302_supermart;

public class OrdinaryTruck extends Truck {
	
	private final int CAPACITY = 1000;

	/**
	 * Constructor for OrdinaryTruck. Has a capacity of 1000 items and no temperature sensitive
	 * items can be stored in an ordinary truck.
	 * @throws DeliveryException Throws when an item with a temperature property is in the truck
	 * or the truck does not have enough room for the item.
	 * @throws StockException
	 */
	public OrdinaryTruck() throws DeliveryException, StockException {
		super();
		// Check if we have any cold stuff in here
		for (Item it : getStock().getItems().keySet()) {
			if (it.getTemperature() != null) {
				throw new DeliveryException("");
			}
		}
	}

	/**
	 * Returns the cost of an ordinary truck using the specified formula that is 
	 * dependent upon the number of items within the truck. Also find the cost of
	 * the stock within the truck.
	 * @return Returns the truck cost as a double.
	 */
	@Override
	public double getCost() {
		double truckCost = 0;
		truckCost = 750 + (0.25 * getCurrentCapacity()) + (getStock().getCost());
		return truckCost;	
	}

	/**
	 * Returns the greatest number of items that this truck can hold.
	 */
	@Override
	public int getCapacity() {
		// TODO Auto-generated method stub
		return CAPACITY;
	}

	/**
	 * Adds an item to a truck, ensuring that the item is not temperature dependent (i.e. is a dry good)
	 * and there is enough room on the truck to add these items.
	 */
	@Override
	public void addItem(Item item, int quantity) throws DeliveryException, StockException {
		if ((this.getCapacity() - this.getCurrentCapacity()) >= quantity && item.getTemperature() == null) {
			getStock().addItem(item, quantity);
		} 
		else {
			throw new DeliveryException("Attempted to add item to Ordinary Truck. Room left on Truck: " 
									+ (this.getCapacity() - this.getCurrentCapacity()) 
									+ "\nItem: \tquantity: " + quantity 
									+ ", \ttemperature: " + item.getTemperature());
		}
	}

	/**
	 * Returns the type of truck as a string.
	 */
	@Override
	public String getName() {
		return "Ordinary";
	}
}
