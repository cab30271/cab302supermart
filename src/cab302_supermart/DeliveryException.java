package cab302_supermart;

public class DeliveryException extends Exception {
	
	public DeliveryException(String string) {
		super(string);
	}
}
