package cab302_supermart;

public class CSVFormatException extends Exception {

	public CSVFormatException(String message) {
		super(message);
	}
}
