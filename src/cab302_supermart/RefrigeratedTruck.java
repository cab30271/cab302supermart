/**
 * Implementation of RefrigeratedTruck
 * @author Joshua Cao
 */

package cab302_supermart;

public class RefrigeratedTruck extends Truck {
	
	private final int CAPACITY = 800;
	private int temperature;
	
	private static int MAX_TEMPERATURE = 10;
	private static int MIN_TEMPERATURE = -20;

	/**
	 * Constructor for RefrigeratedTruck
	 * @param temperature - Temperature of truck.
	 * @throws DeliveryException
	 * @throws StockException 
	 */
	public RefrigeratedTruck(int temperature) throws DeliveryException, StockException {
		super();
		// So if there's no temperature items, temperature=Integer.MAX_VALUE and we'll trip here
		if (temperature > MAX_TEMPERATURE || temperature < MIN_TEMPERATURE) {
			throw new DeliveryException("Item temperature out of bounds");
		}
		else {this.temperature = temperature;}
	}
	
	/*
	 * (non-Javadoc)
	 * @see cab302_supermart.Truck#getCost()
	 */
	@Override
	public double getCost() {
		double truckCost = 0;
		truckCost = 900 + (200 * Math.pow(0.7, (temperature/5.0))) + getStock().getCost();
		return truckCost;
	}

	/*
	 * (non-Javadoc)
	 * @see cab302_supermart.Truck#getCapacity()
	 */
	@Override
	public int getCapacity() {
		return CAPACITY;
	}
	
	/**
	 * @author Joshua Cao
	 * @return Temperature of the truck
	 */
	public int getTemperature() {
		return temperature;
	}
	
	/**
	 * Adds an item to a refrigerated truck. Checking that there is room on the truck and that 
	 * the required temperature of the item is not less than the current temperature of the truck.
	 */
	@Override
	public void addItem(Item item, int quantity) throws DeliveryException, StockException {
		if ( (item.getTemperature() == null) || 
				(this.getCapacity() - this.getCurrentCapacity()) >= quantity 
				&& (item.getTemperature() >= this.getTemperature()
				)) {
			getStock().addItem(item, quantity);
		} 
		else if((this.getCapacity() - this.getCurrentCapacity()) < quantity){
			throw new DeliveryException("Too many items in truck.");
		}
	}

	/**
	 * Returns the type of truck as a string.
	 */
	@Override
	public String getName() {
		return "Refrigerated";
	}
	
	/**
	 * Updates the refrigerated truck temperature to the value supplied.
	 * @param temperature New temperature required for the truck.
	 */
	public void updateTemperature(int temperature) {
		this.temperature = temperature;	
	}


}
