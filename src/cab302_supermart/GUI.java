/**
 * GUI Class.
 * @author Joshua Cao
 */

package cab302_supermart;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

public class GUI extends JPanel implements ActionListener {
	
	private final static String windowTitle = "SuperMart Control Centre";
	
	//Create a file chooser
	final JFileChooser fc = new JFileChooser();
	
	// Create the window.
	static JFrame frame = new JFrame(windowTitle);
	
	ArrayList<JButton> buttonList;
	
	private static Store store;
	
	private StockTableModel tableModel;
	private JTextPane infobox;
	
	/**
	 * Creates the GUI.
	 * @author Joshua Cao
	 */
	public void createGUI() {
		JFrame.setDefaultLookAndFeelDecorated(true);
		
		frame.setLayout(new GridBagLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		changeNameAction();		// initial name choice
		
		/****
		 * Big Title Text
		 */
		// Component
		JLabel headerLabel = new JLabel("SuperMart Manager Control Centre.");
		headerLabel.setFont(new Font("Helvetica", Font.BOLD, 32));
		
		// Constraints
		GridBagConstraints headerConstraints = new GridBagConstraints();
		headerConstraints.gridx = 0;
		headerConstraints.gridy = 0;
		headerConstraints.gridwidth = GridBagConstraints.REMAINDER;
		headerConstraints.gridheight = 1;
		headerConstraints.anchor = GridBagConstraints.LINE_START;
		
		frame.add(headerLabel, headerConstraints);
		
		/****
		 * Table + ScrollPane
		 */
		//Components
		tableModel = new StockTableModel(store);
		JTable table = new JTable(tableModel);
		table.setAutoCreateRowSorter(true);
		table.setFillsViewportHeight(true);
		// Make the scroll thing.
		JScrollPane scroll = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		// Renderers
		TableColumnModel columnModel = table.getColumnModel();
		// 1 and 2 are currencies
		columnModel.getColumn(1).setCellRenderer(new CurrencyRenderer());
		columnModel.getColumn(2).setCellRenderer(new CurrencyRenderer());
		
		// Constraints
		GridBagConstraints tableConstraints = new GridBagConstraints();
		tableConstraints.gridx = 0;
		tableConstraints.gridy = 1;
		tableConstraints.gridwidth = 5;
		tableConstraints.gridheight = GridBagConstraints.REMAINDER;
		tableConstraints.weighty = 1.0;
		tableConstraints.weightx = 1.0;
		tableConstraints.fill = GridBagConstraints.BOTH;
		
		frame.add(scroll, tableConstraints);
		
		/**
		 * Infobox.
		 */
		// Component.
		infobox = new JTextPane();
		infobox.setContentType("text/html");
		infobox.setEditable(false);
		// temporary hardcode this.
		updateInfobox();
		
		// Constraints.
		GridBagConstraints infoboxConstraints = new GridBagConstraints();
		infoboxConstraints.gridx = 5;
		infoboxConstraints.gridy = 1;
		infoboxConstraints.gridwidth = GridBagConstraints.REMAINDER;
		infoboxConstraints.gridheight = 1;
		tableConstraints.fill = GridBagConstraints.BOTH;

		frame.add(infobox, infoboxConstraints);
		
		/**
		 * Buttons sublayout.
		 */
		// Panel.
		JPanel buttonLayout = new JPanel();
		buttonLayout.setLayout(new BoxLayout(buttonLayout, 1));
		
		// Constraints.
		GridBagConstraints buttonLayoutConstraints = new GridBagConstraints();
		buttonLayoutConstraints.gridx = 5;
		buttonLayoutConstraints.gridy = 2;
		buttonLayoutConstraints.gridwidth = GridBagConstraints.REMAINDER;
		buttonLayoutConstraints.gridheight = GridBagConstraints.REMAINDER;
		buttonLayoutConstraints.fill = GridBagConstraints.BOTH;
		
		frame.add(buttonLayout, buttonLayoutConstraints);
		
		/**
		 * Buttons (in sublayout)
		 */
		// Array of button texts we want, and their command strings.
		String[][] button_texts = {
				{"Load Item Properties", "load_properties"},
				{"Export Manifest", "export_manifest"},
				{"Load Manifest", "load_manifest"},
				{"Load Sales Log","sales_log"},
				{"Change Name","change_name"}
		};
		
		// Make some buttons!
		buttonList = new ArrayList<JButton>();
		
		for (int i=0; i<button_texts.length; i++) {
			buttonList.add(new JButton(button_texts[i][0]));			// button name
			buttonList.get(i).setActionCommand(button_texts[i][1]);	// command string
			buttonList.get(i).addActionListener(this);				// hook up listener
			buttonLayout.add(buttonList.get(i));						// add to boxlayout
			// Disable all buttons but the first.
			if (i > 0) {
				buttonList.get(i).setEnabled(false);
			}
		}
		frame.setLocation(new Point(200, 200));
		frame.pack();
		frame.setVisible(true);
	}
	
	/**
	 * Retrieve a file using a user-friendly dialog box.
	 * @param windowTitle - the title of the window we present
	 * @param command - the word on the button they click
	 * @return - A file object (if they selected one), or null (if they canceled)
	 * @author Joshua Cao
	 */
	public File getFile(String windowTitle, String command) {
		fc.setDialogTitle(windowTitle);
		int returnVal = fc.showDialog(frame, command);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile();
		} else {
			return null;
		}
	}
	
	/**
	 * Event listener for when they click on a button.
	 * All of the buttons are actually handled in this same function.
	 * @author Joshua Cao
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		File targetFile;
		switch (command) {
			case "load_properties":
				targetFile = getFile("Load Item Properties", "Open");
				if (targetFile != null) {
					try {
						store.loadItemProperties(targetFile);
						// Disable this button and enable the other ones
						for (JButton button : buttonList) {
							button.setEnabled(!button.isEnabled());
						}
					} catch (CSVFormatException | StockException e1) {
						store.resetStoreStock();						
						showMessage(e1.getMessage());
					}
				}
				break;
			case "export_manifest":
				targetFile = getFile("Export Manifest", "Save");
				if (targetFile != null) {
					try {
						store.exportManifest(targetFile);
						showMessage("Export Successful!");
					} catch (StockException | CSVFormatException | DeliveryException e1) {
						// TODO Auto-generated catch block
						showMessage(e1.getMessage());
					}
				}
				break;
			case "load_manifest":
				targetFile = getFile("Load Manifest", "Open");
				if (targetFile != null) {
					try {
						store.loadManifest(targetFile);
					} catch (DeliveryException | StockException | CSVFormatException err) {
						showMessage(err.getMessage());
					}
				}
				break;
			case "sales_log":
				targetFile = getFile("Load Sales Log", "Open");
				if (targetFile != null) {
					try {
						store.loadSalesLog(targetFile);
					} catch (StockException | CSVFormatException e1) {
						// TODO Auto-generated catch block
						showMessage(e1.getMessage());
					}
				}
				break;
			case "change_name":
				changeNameAction();
				break;
		}
		tableModel.fireTableDataChanged();
		updateInfobox();
	}
	
	public void changeNameAction() {
		String targetString = JOptionPane.showInputDialog("Please Enter Store Name");
		try {
			store.changeName(targetString);
		} catch (NameException e1) {
			// TODO Auto-generated catch block
			showMessage(e1.getMessage());
		}
	}
	
	/**
	 * Updates the Infobox to match any new information.
	 * Also, if the Capital is below 0, it will display it in red.
	 */
	public void updateInfobox() {
		String currencyString = new DecimalFormat("0.00").format(GUI.store.getCapital());
		String formatString;
		// display capital in red if we're below 0
		if (store.getCapital() < 0) {
			formatString = "<p>Name: %s</p><p><b color='red'>Capital: $%s</b></p>";
		} else {
			formatString = "<p>Name: %s</p><p>Capital: $%s</p>";
		}
		infobox.setText(String.format(formatString, store.getName(), currencyString));
		
	}
	
	/**
	 * Displays a message with the specified string.
	 * @param message - the string which the error is on
	 */
	public void showMessage(String message) {
		JOptionPane.showMessageDialog(frame, message);
	}

	/**
	 * Construct a GUI.
	 * @param store - Store object that this GUI represents.
	 */
	public GUI(Store store) {
		GUI.store = store;
		createGUI();
	}

}

/**
 * A TableModel for presenting the inventory of the Store as a JTable.
 * @author Joshua Cao
 *
 */
class StockTableModel extends AbstractTableModel {
	
	private Store store;
	private String[] columns = {
		"Name",
		"Cost",
		"Price",
		"Reorder Point",
		"Reorder Amount",
		"Temperature",
		"Quantity"
	};
	
	/**
	 * Construct a Table Model.
	 * @param store - store object which this table model represents.
	 */
	public StockTableModel(Store store) {
		this.store = store;
	}
	
	/**
	 * Return the name of the column at a given index.
	 */
	@Override 
	public String getColumnName(int columnIndex) {
		return columns[columnIndex];
	}

	/**
	 * Return the number of rows in the model.
	 */
	@Override
	public int getRowCount() {
		return store.getStock().getItems().size();
	}

	/**
	 * Return the number of columns in the model.
	 */
	@Override
	public int getColumnCount() {
		// Fields of Item + quantity
		return 7;
	}

	/**
	 * Return the value of the Model at any row+column.
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// this might break horribly depending on what a hashmap does here
		Item targetItem = (Item) store.getStock().getItems().keySet().toArray()[rowIndex];
		switch (columnIndex) {
			case 0: return targetItem.getName();
			case 1: return targetItem.getCost();
			case 2: return targetItem.getPrice();
			case 3: return targetItem.getReorderPoint();
			case 4: return targetItem.getReorderAmount();
			case 5: return targetItem.getTemperature();
			case 6: return store.getStock().getQuantity(targetItem);
			default: return null;
		}
	}
	
	/**
	 * Return the class of the given column index (e.g. string, number)
	 * This allows the JTable sorter to correctly identify what to sort by.
	 */
	@Override
	public Class getColumnClass(int column) {
		switch (column) {
			case 0: return String.class;
			case 1: return Double.class;
			case 2: return Double.class;
			case 3: return Integer.class;
			case 4: return Integer.class;
			case 5: return Integer.class;
			case 6: return Integer.class;
			default: return String.class;
		}
	}
	
}

/**
 * A renderer which formats simple currencies.
 * @author Joshua Cao
 *
 */
class CurrencyRenderer extends DefaultTableCellRenderer
{
	/**
	 * Sets the value of the given Object before being sent to the renderer.
	 */
    public void setValue(Object value)
    {
        try
        {
            if (value != null)
                value = "$" + value.toString();
        }
        catch(IllegalArgumentException e) {}
 
        super.setValue(value);
    }
}
