/**
 * Tests for the RefrigeratedTruck Class
 * @author Nathan Morrison
 *
 */
package cab302_supermart;

import static org.junit.Assert.*;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Before;
import org.junit.Test;


public class RefrigeratedTruckTest {

	//Set up testing helpers
	private static Random random = new Random();
	
	private static int MAX_TEMPERATURE = 10;
	private static int MIN_TEMPERATURE = -20;
	private static int MAX_CAPACITY = 800;
	
	public static int randInt(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}
	
	RefrigeratedTruck rTruck;
	
	@Before
	public void setUp() throws Exception {
		rTruck = null;
	}
	
	
	@Test public void setUpRefrigeratedTruckWithStock() throws DeliveryException, StockException {
		int temperature = randInt(MIN_TEMPERATURE, MAX_TEMPERATURE);
	
		rTruck = new RefrigeratedTruck(temperature);
	}
	
	@Test public void checkTruckTemperatureIsCorrect() throws DeliveryException, StockException {
		int temperature = randInt(MIN_TEMPERATURE, MAX_TEMPERATURE);
		Item item = new Item("Random Item @Allowable Temperature", 1, 2, 300, 600, temperature);		
		rTruck = new RefrigeratedTruck(temperature+1);
		rTruck.addItem(item, 10);
		rTruck.updateTemperature(temperature);
		assertEquals(temperature, rTruck.getTemperature());
	}
	
	@Test (expected = DeliveryException.class)
	public void attemptRefrigeratedTruckAboveMaxTemp() throws DeliveryException, StockException {
		int temperature = MAX_TEMPERATURE + 1;
		
		rTruck = new RefrigeratedTruck(temperature);
	}
	
	@Test (expected = DeliveryException.class)
	public void attemptRefrigeratedTruckBelowMinTemp() throws DeliveryException, StockException {
		int temperature = MIN_TEMPERATURE - 1;
		
		rTruck = new RefrigeratedTruck(temperature);
	}
	
	@Test public void getCostRefrigeratedTruck() throws DeliveryException, StockException {
		int temperature = randInt(MIN_TEMPERATURE, MAX_TEMPERATURE);
		int itemQuantity = 10;
		
		Item item = new Item("Random Item @Allowable Temperature", 1, 2, 300, 600, temperature);
		
		double cost = 900 + (200 * (Math.pow(0.7, temperature/5.0))) + (item.getCost() * itemQuantity) ;
		
		rTruck = new RefrigeratedTruck(temperature);
		rTruck.addItem(item, itemQuantity);
		
		assertEquals(cost, rTruck.getCost(),0);
	}
	
	@Test public void getStockRefrigeratedTruck() throws DeliveryException, StockException {
		int temperature = randInt(MIN_TEMPERATURE, MAX_TEMPERATURE);
		Item item = new Item("Random Item @Allowable Temperature", 1, 2, 300, 600, temperature);
		Stock stock = new Stock();
		stock.addItem(item, 10);
		
		rTruck = new RefrigeratedTruck(temperature);
		rTruck.addItem(item, 10);
		
		assertEquals(stock.getItems(), rTruck.getStock().getItems());
	}
	
	@Test (expected = DeliveryException.class)
	public void attemptAddMoreThanCapacityRefrigeratedTruck() throws DeliveryException, StockException {
		int temperature = randInt(MIN_TEMPERATURE, MAX_TEMPERATURE);
		
		Item item = new Item("Random Item @Allowable Temperature", 1, 2, 300, 600, temperature);
		
		rTruck = new RefrigeratedTruck(temperature);
		rTruck.addItem(item, MAX_CAPACITY + 1);
	}
	
	

}
