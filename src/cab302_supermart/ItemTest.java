/**
 * Tests for the Item class
 * @author Joshua Cao
 */

package cab302_supermart;

import static org.junit.Assert.*;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Before;
import org.junit.Test;

public class ItemTest {
	
	// Set up testing helpers
	private static Random random = new Random();
	private static String[] names = new String[] {
			"Milk Chocolate",
			"White Chocolate",
			"Dark Chocolate",
			"Delicious Fudge",
			"Scumptious Ice Cream",
			"Amazing Apple Pie",
			"Tasty Rice Pudding",
			"More-ish Macarons",
			"Crazy Cheesecake",
		};
	private static double MAX_PRICE = 400.0;
	private static double MIN_PRICE = 0.05;
	
	Item item;
		
	/**
	 * Return a random integer.
	 * @param min - minimum bound
	 * @param max - maximum bound
	 * @return a random integer j such that min is less than or equal j is less than or equal to max
	 */
	public static int randInt(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}
	
	/**
	 * Return a random double.
	 * @param min - minimum bound
	 * @param max - maximum bound
	 * @return a random double j such that min is less than or equal to j is less than max
	 */
	public static double randDouble(double min, double max) {
		return ThreadLocalRandom.current().nextDouble(min, max);
	}
	
	@Before
	public void setUp() throws Exception {
		item = null;
	}

	
	//Test that the constructor of Item contains 6 arguments.
	@Test
	public void ItemHasCorrectConstructorTest() throws StockException {
		double cost = randDouble(MIN_PRICE, MAX_PRICE);
		// profit margin
		double price = cost + 1.0;
		item = new Item(
			names[random.nextInt(names.length)],
			cost,
			price,
			randInt(1, 10),
			randInt(10, 20),
			randInt(-20, 5)
		);
	}
	

	//Test that a Item object allows null as a temperature value.
	@Test
	public void ItemCanTakeNullAsTemperatureTest() throws StockException {
		item = new Item(
			names[random.nextInt(names.length)],
			5.24,
			4.24,
			10,
			20,
			null
		);
	}
	
	
	//Test that getName() returns the name of the Item.
	@Test
	public void ItemGetNameTest() throws StockException {
		String selectedName = names[random.nextInt(names.length)];
		item = new Item(
			selectedName,
			5.24,
			4.24,
			10,
			20,
			null
		);
		assertEquals(selectedName, item.getName());
	}
	
	
	// Test that getCost() returns the cost of the Item.
	@Test
	public void ItemGetCostTest() throws StockException {
		double selectedCost = randDouble(MIN_PRICE, MAX_PRICE);
		item = new Item(
			names[random.nextInt(names.length)],
			selectedCost,
			4.24,
			10,
			20,
			null
		);
		assertEquals(selectedCost, item.getCost(), 0);
	}
	
	
	
	// Test that getPrice() returns the price of the item.
	@Test
	public void ItemGetPriceTest() throws StockException {
		double selectedPrice = randDouble(MIN_PRICE, MAX_PRICE);
		item = new Item(
			names[random.nextInt(names.length)],
			5.434,
			selectedPrice,
			10,
			20,
			null
		);
		assertEquals(selectedPrice, item.getPrice(), 0);
	}
	
	
	// Test that getReorderPoint returns the reorder point of the item.
	@Test
	public void ItemGetReorderPointTest() throws StockException {
		int selectedPoint = randInt(1, 30);
		item = new Item(
			names[random.nextInt(names.length)],
			5.434,
			6.434,
			selectedPoint,
			20,
			null
		);
		assertEquals(selectedPoint, item.getReorderPoint());
	}
	
	
	// Test that getReorderAmount returns the reorder amount of the item.
	@Test
	public void ItemGetReorderAmountTest() throws StockException {
		int selectedAmount = randInt(30, 40);
		item = new Item(
			names[random.nextInt(names.length)],
			5.434,
			6.434,
			20,
			selectedAmount,
			null
		);
		assertEquals(selectedAmount, item.getReorderAmount());
	}
	
	
	// Test that getTemperature returns the temperature of the item.
	@Test
	public void ItemGetTemperatureTest() throws StockException {
		Integer selectedTemp = randInt(-20, 0);
		item = new Item(
			names[random.nextInt(names.length)],
			5.434,
			6.434,
			20,
			30,
			selectedTemp
		);
		assertEquals(selectedTemp, item.getTemperature());
	}
	
	
	// Test that getTemperature returns null when no temperature is required.
	@Test
	public void ItemGetTemperatureWhenNullTest() throws StockException {
		item = new Item(
			names[random.nextInt(names.length)],
			5.434,
			6.434,
			20,
			30,
			null
		);
		assertEquals(null, item.getTemperature());
	}
	
	// Test that Item throws a StockException if the given temperature
	//value is out of the acceptable range.
	@Test (expected = StockException.class)
	public void ItemThrowsStockExceptionWithIllegalTemperaturesTest() throws StockException {
		item = new Item(
				names[random.nextInt(names.length)],
				5.434,
				6.434,
				20,
				30,
				-40
			);
	}
	
	// Test that the equals method returns True for two items of identical parameters.
	@Test
	public void ItemEqualsMethodReturnsTrueOnIdenticalParametersTest() throws StockException {
		item = new Item(
				"Fried Lemonade",
				5.434,
				6.434,
				20,
				30,
				null
			);
		Item item2 = new Item(
				"Fried Lemonade",
				5.434,
				6.434,
				20,
				30,
				null
			);
		assertTrue(item.equals(item2));
		assertTrue(item2.equals(item));
	}
	
	// Test that the equals method returns False for two items of different parameters.
	@Test
	public void ItemEqualsMethodReturnsFalseOnDifferentParametersTest() throws StockException {
		item = new Item(
				"Fried Lemonade",
				5.434,
				6.434,
				20,
				30,
				null
			);
		Item item2 = new Item(
				"Fried Lemonade",
				5.434,
				6.434,
				21,
				30,
				null
			);
		assertFalse(item.equals(item2));
		assertFalse(item2.equals(item));
	}
	
	// Test that the hashCode() method returns the same number for two items of the same parameters.
	@Test
	public void ItemHashCodeIdenticalTest() throws StockException {
		item = new Item(
				"Profiterole",
				5.2,
				6.8,
				40,
				80,
				0
			);
		Item item2 = new Item(
				"Profiterole",
				5.2,
				6.8,
				40,
				80,
				0
			);
		assertEquals(item.hashCode(), item2.hashCode());
	}
	
	// Test that the hashCode() method returns different numbers for different items.
	@Test
	public void ItemHashCodeDifferentTest() throws StockException {
		item = new Item(
				"Profiterole",
				5.2,
				6.8,
				40,
				80,
				0
			);
		Item item2 = new Item(
				"Profiteroles",
				7,
				6.8,
				40,
				80,
				0
			);
		assertTrue(item.hashCode() != item2.hashCode());
	}
	
	// Test that the hashCode() method returns the same numbers for items with
	// different parameters, but the same name.
	@Test
	public void ItemHashCodeIdenticalWithSameNameButDifferentParametersTest() throws StockException {
		item = new Item(
				"Profiterole",
				5.2,
				6.8,
				40,
				80,
				0
			);
		Item item2 = new Item(
				"Profiterole",
				723423,
				34343,
				4023,
				80,
				null
			);
		assertEquals(item.hashCode(), item2.hashCode());
	}
}
