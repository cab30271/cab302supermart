/**
 * Tests for the OrdinaryTruck class.
 * @author Nathan Morrison
 */
package cab302_supermart;

import static org.junit.Assert.*;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Nathan Morrison
 *
 */
public class OrdinaryTruckTest {

	//Set up testing helpers
	private static Random random = new Random();
	
	private static int MAX_CAPACITY = 1000;
	
	public static int randInt(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}
	
	OrdinaryTruck oTruck;
	
	@Before
	public void setUp() throws Exception {
		oTruck = null;
	}
	
	@Test public void setUpOrdinaryTruck() throws DeliveryException, StockException {		
		oTruck = new OrdinaryTruck();
	}

	@Test (expected = DeliveryException.class)
	public void attemptOrdinaryTruckWithColdStock() throws DeliveryException, StockException {
		Item item = new Item("Random Item", 1, 2, 300, 600, 0);
		
		oTruck = new OrdinaryTruck();
		
		oTruck.addItem(item, 10);
	}
	
	@Test public void getCostOrdinaryTruck() throws DeliveryException, StockException {
		Item item = new Item("Random Item", 1, 2, 300, 600, null);
		int quantityItems = 10;
		
		double expectedCost = 750 + (0.25 * quantityItems) + (item.getCost() * quantityItems);
		
		oTruck = new OrdinaryTruck();
		
		oTruck.addItem(item, quantityItems);
		
		assertEquals(expectedCost,oTruck.getCost(),0);
	}
	
	@Test
	public void testAddItem() throws DeliveryException, StockException {
		Item item = new Item("Random Item", 1, 2, 300, 600, null);
		Stock stock = new Stock();
		int quantityItems = 10;
		stock.addItem(item, quantityItems);
		
		oTruck = new OrdinaryTruck();
		
		oTruck.addItem(item, quantityItems);
		assertEquals(oTruck.getStock().getItems(), stock.getItems());		
	}
	
	@Test (expected = DeliveryException.class)
	public void testAddTooManyItems() throws DeliveryException, StockException {
		Item item = new Item("Random Item", 1, 2, 300, 600, null);
		int quantityItems = MAX_CAPACITY+1;		
		oTruck = new OrdinaryTruck();		
		oTruck.addItem(item, quantityItems);	
	}
	
}
