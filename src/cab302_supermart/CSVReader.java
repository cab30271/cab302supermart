/**
 * Simple CSV writing and reading library.
 * Assumes delimiters are ",", and does not handle any escaped characters, etc.
 * (So we're assuming fields won't have the "," character in them)
 * @author Joshua Cao
 */

package cab302_supermart;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.lang.String;

public class CSVReader implements Iterable<ArrayList<String>>{
	
	private InputStream s;
	private BufferedReader br;
	
	/**
	 * Create a new CSVReader for the specified file.
	 * @author Joshua Cao
	 * @throws CSVFormatException When the provided CSV is not in the required grammar
	 * of the inventory management system.
	 * @param file. CSV File to be read.
	 */
	public CSVReader(File file) throws CSVFormatException {
		// Set up the file writer.
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		} catch (FileNotFoundException e) {
			throw new CSVFormatException("File does not exist.");
		}
	}
	
	/**
	 * Read a CSV file.
	 * @return An iterator of arrays of strings, each being one line of the file.
	 * @author Joshua Cao
	 */
	@Override
	public Iterator<ArrayList<String>> iterator() {

		Iterator<ArrayList<String>> it = null;
		
		it = new Iterator<ArrayList<String>>() {

			String line = null;  

			@Override
			public boolean hasNext() {
				// hasNext is always called before next, so it's safe here to
				// just grab the next line and check if it's null.
				try {
					line = br.readLine();
					if (line == null) {
						br.close();
						return false;
					} else {
						return true;
					}
				} catch (IOException e) {
					// TODO: throw CSV exception here?
					return false;
				}
			}

			@Override
			public ArrayList<String> next() {
				// We got the next line in hasNext already.
				// split on ",", then return as an ArrayList.
				return new ArrayList<String>(Arrays.asList(line.split(",")));
			}
			
		};
		return it;
	}

}
