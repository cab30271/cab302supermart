/**
 * Truck - abstract class.
 * Not used directly; use OrdinaryTruck or RefrigeratedTruck.
 * @author Joshua Cao
 */

package cab302_supermart;

public abstract class Truck {

	private Stock stock;
	public Truck() throws StockException {
		this.stock = new Stock();
	}
	
	public Stock getStock() {
		return this.stock;
	}
	
	/*
	 * Returns the Cost of the Truck and the stock inside.
	 */
	public abstract double getCost();
	
	/*
	 * Returns max capacity of the Truck
	 */
	public abstract int getCapacity();
	
	/*
	 * Returns the name of the Truck (for Manifest exporting).
	 */
	public abstract String getName();
	
	/**
	 * @author Joshua Cao
	 * @return Current capacity in the truck.
	 */
	public int getCurrentCapacity() {
		int numberOfItems = 0;
		for (int i : stock.getItems().values()) {
			numberOfItems += i;
		}
		return numberOfItems;
	}
	
	/**
	 * Exports the truck. This is called by Manifest, and shouldn't be called directly. 
	 * @param file - CSVWriter to write to.
	 */
	public void export(CSVWriter file) {
		file.writeLine(">" + getName());
		stock.export(file);
	}
	
	
	/**
	 * @author Joshua Cao
	 * @return Capacity remaining in the truck
	 */
	public int getCapacityRemaining() {
		return getCapacity() - getCurrentCapacity();
	}
	
	
	public abstract void addItem(Item item, int quantity) throws DeliveryException, StockException;

	

}
