/**
 * Item class impelementation.
 * @author Nathan Morrison
 */
package cab302_supermart;

public class Item {
	private String item;
	private double cost;
	private double price;
	private int reorderPoint;
	private int reorderAmount;
	private Integer temperature;
	
	/**
	 * Item class constructor for Supermart.
	 * @author Nathan Morrison
	 * @param item: name of item
	 * @param cost: cost price of item
	 * @param price: sale price of item
	 * @param reorderPoint: When the quantity reaches this value or less, reorder item.
	 * @param reorderAmount: When the quantity reaches the reorderPoint, reorder this many items.
	 * @param temperature: Temperature in �C that must be maintained for the item to not perish. 
	 * Not all items are temperature controlled, and so they will not need a temperature. 
	 * Please note the use of the class Integer and not the primitive type int, allowing for null values.
	 * @throws StockException An exception where something has gone wrong with stock. Usually to do with
	 * invalid properties.
	 */
	public Item(String item, double cost, double price, int reorderPoint, int reorderAmount, Integer temperature) throws StockException {
		this.item = item;
		
		if (cost <= 0) {
			throw new StockException("Cost of " + item + "is out of range at: " + cost);
		}
		else {
			this.cost = cost;
		}
		
		if (price <= 0) {
			throw new StockException("Price of " + item + "is out of range at: " + price);
		}
		else {
			this.price = price;
		}
		
		if (reorderPoint < 0) {
			throw new StockException("Reorder Point of " + item + "is out of range at: " + reorderPoint);
		}
		else {
			this.reorderPoint = reorderPoint;
		}
		
		if (reorderAmount < 1) {
			throw new StockException("Reorder Amount of " + item + "is out of range at: " + reorderAmount);
		}
		else {
			this.reorderAmount = reorderAmount;
		}
		

		if (temperature != null && (temperature > 10 || temperature < -20)) {
				throw new StockException("Temperature value for " + item + " is out of range.");
		}
		else {
			this.temperature = temperature;
		}
		
	}

		
	/**
	 * @return returns the item name.
	 */
	public String getName() {
		return this.item;
	}

	/**
	 * @return returns the item cost.
	 */
	public double getCost() {
		return this.cost;
	}

	/**
	 * @return returns the item sale price.
	 */
	public double getPrice() {
		return this.price;
	}

	/**
	 * @return returns the quantity at which the given item needs to be reordered at.
	 */
	public int getReorderPoint() {
		return this.reorderPoint;
	}
	
	/**
	 * @return returns the number of items that are to be reordered.
	 */
	public int getReorderAmount() {
		return this.reorderAmount;
	}

	/**
	 * @return returns the temperature at which the item must be stored.
	 * A null value indicates that the item does not need to be stored at a specific temperature.
	 */
	public Integer getTemperature() {
		return this.temperature;
	}

	/**
	 * @author Joshua Cao
	 * @return If this and a different item are equal.
	 * This is literally only here for tests
	 */
	@Override
	public boolean equals(Object other) {
		Item otherItem = (Item)other;
		return (
			getName() == otherItem.getName() &&
			getCost() == otherItem.getCost() &&
			getPrice() == otherItem.getPrice() &&
			getReorderPoint() == otherItem.getReorderPoint() &&
			getReorderAmount() == otherItem.getReorderAmount() &&
			getTemperature() == otherItem.getTemperature());
	}
	
	/**
	 * @author Joshua Cao
	 * @return A unique hashCode for this Item, for the purposes of Stock's internal HashMap.
	 */
	@Override
	public int hashCode() {
		// Note the collisions between items of the same name, but different parameters.
		// This is intentional: we assume that they are referring to the same item, but just
		// at different prices, etc. (e.g. the item is discounted.)
		return item.hashCode();
	}

	}
