/**
 * Tests for the store class
 * @author Nathan Morrison & Joshua Cao
 */
package cab302_supermart;

import static org.junit.Assert.*;

import java.io.File;
import org.junit.Before;
import org.junit.Test;

public class StoreTest {
	
	Store store;
	private final double INITIAL_CAPITAL = 100000.0;
	Item rice;
	Item bread;
	Item asparagus;
	Item mix;
	Item ice_cream;
	
	@Before
	public void setUp() throws Exception {
		store = null;
		
		rice = new Item("rice", 2, 3, 225, 300, null);
		bread = new Item("bread", 2, 3, 125, 200, null);
		asparagus = new Item("asparagus", 2, 4, 175, 275, 8);
		mix = new Item("frozen vegetable mix", 5, 8, 225, 325, -12);
		ice_cream = new Item("ice cream",8,14,175,250,-20);
	}
	
	@Test
	public void StoreHasCorrectConstructorTest() throws StockException {
		String storeName = new String("Supermart");
		store = new Store(storeName, INITIAL_CAPITAL);	
	}

	@Test
	public void TestloadItemProperties() throws StockException, CSVFormatException {
		File testFile = new File("test_data.csv");
		String storeName = new String("Supermart");
		store = new Store(storeName, INITIAL_CAPITAL);
		store.loadItemProperties(testFile);
		
		Stock expectedStock = new Stock();
		
		expectedStock.addItem(rice, 0);
		expectedStock.addItem(bread, 0);
		expectedStock.addItem(asparagus, 0);
		expectedStock.addItem(mix, 0);
		expectedStock.addItem(ice_cream, 0);
		
		assertEquals(expectedStock.getItems().keySet().toString(),store.getStock().getItems().keySet().toString());		
	}
	
	@Test
	public void TestgetItemByName() throws StockException, CSVFormatException {

		File testFile = new File("test_data.csv");
		String storeName = new String("Supermart");
		store = new Store(storeName, INITIAL_CAPITAL);
		store.loadItemProperties(testFile);
		
		Stock expectedStock = new Stock();
		
		expectedStock.addItem(rice, 0);
		expectedStock.addItem(bread, 0);
		expectedStock.addItem(asparagus, 0);
		expectedStock.addItem(mix, 0);
		expectedStock.addItem(ice_cream, 0);
		
		assertEquals(store.getItemByName("rice").getName(),rice.getName());
		assertEquals(store.getItemByName("bread").getName(), bread.getName());
		assertEquals(store.getItemByName("asparagus").getName(), asparagus.getName());
		assertEquals(store.getItemByName("frozen vegetable mix").getName(), mix.getName());
		assertEquals(store.getItemByName("ice cream").getName(), ice_cream.getName());
	}

	@Test
	public void TestgetCapital() throws StockException {
		String storeName = new String("Supermart");
		store = new Store(storeName, INITIAL_CAPITAL);
		
		assertEquals(INITIAL_CAPITAL,store.getCapital(),0);
	}
	
	@Test
	public void TestgetStoreName() throws StockException {
		String storeName = new String("Supermart");
		store = new Store(storeName, INITIAL_CAPITAL);
		
		assertEquals(storeName,store.getName());
	}
}

