package cab302_supermart;

public class NameException extends Exception {
	public NameException(String message) {
		super(message);
	}
}
