/**
 * Tests for the truck class
 * @author Nathan Morrison
 */
package cab302_supermart;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TruckTest {

	Truck aTruck;
	
	@Before
	public void setUp() throws Exception {
		aTruck = null;
	}
	
	@Test
	public void testGetNameOrdinary() throws DeliveryException, StockException {
		aTruck = new OrdinaryTruck();
		
		assertEquals(aTruck.getName(), "Ordinary");
	}

	@Test
	public void testGetNameRefrigerated() throws DeliveryException, StockException {
		aTruck = new RefrigeratedTruck(10);
		
		assertEquals(aTruck.getName(), "Refrigerated");
	}

	@Test
	public void testGetCapacityOrdinary() throws DeliveryException, StockException {
		aTruck = new OrdinaryTruck();
		
		assertEquals(aTruck.getCapacity(), 1000);
	}

	@Test
	public void testGetCapacityRefrigerated() throws DeliveryException, StockException {
		aTruck = new RefrigeratedTruck(10);
		
		assertEquals(aTruck.getCapacity(), 800);
	}
	
	@Test
	public void testGetCapacityRemainingOrdinary() throws DeliveryException, StockException {
		aTruck = new OrdinaryTruck();
		
		assertEquals(aTruck.getCapacityRemaining(), 1000);
	}

	@Test
	public void testGetCapacityRemainingRefrigerated() throws DeliveryException, StockException {
		aTruck = new RefrigeratedTruck(10);
		
		assertEquals(aTruck.getCapacityRemaining(), 800);
	}
	

	@Test
	public void testGetCurrentCapacityOrdinary() throws DeliveryException, StockException {
		aTruck = new OrdinaryTruck();
		
		assertEquals(aTruck.getCurrentCapacity(), 0);
	}

	@Test
	public void testGetCurrentCapacityRefrigerated() throws DeliveryException, StockException {
		aTruck = new RefrigeratedTruck(10);
		
		assertEquals(aTruck.getCurrentCapacity(), 0);
	}
}
