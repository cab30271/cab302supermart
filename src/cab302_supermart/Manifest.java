/**
 * Manifest class that optimises Stock orders into trucks.
 * @author Joshua Cao
 */

package cab302_supermart;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class Manifest {

	private List<Truck> trucks;
	
	public Manifest() {
		this.trucks = new ArrayList<Truck>();
	}
	
	/**
	 * Updates this Manifest to fulfill the given stock order in the most optimised fashion.
	 * @param order - the order to fill.
	 * @throws StockException     (Presumably, if this code is good, these exceptions
	 * @throws DeliveryException   shouldn't occur)
	 */
	public void fulfill(Stock order) throws DeliveryException, StockException {
		// Clear the trucks, in case we've used this Manifest before
		this.trucks = new ArrayList<Truck>();
		
		// Sort the items by lowest temperature.
		ArrayList<Item> sortedList = new ArrayList<Item>(order.getItems().keySet());		
		sortedList.sort(new Comparator<Item>() {
			// Comparator to sort by temperature, null is highest
			@Override
			public int compare(Item o1, Item o2) {
				Integer one = o1.getTemperature();
				Integer two = o2.getTemperature();
				// If either are null, the one that is null is always higher.
			    if (one == null ^ two == null) {
			        return (two == null) ? -1 : 1;
			    }
			    // If both are null, then they are equal.
			    if (one == null && two == null) {
			        return 0;
			    }
			    // Otherwise, they are both cold items.
			    return one.compareTo(two);
			}
		});
		
		// Now we've got the items sorted by temperature, so we can start fulfiling the order.
		int currentIndex = 0;		// Current index of sortedList we're working on fulfilling.
		int itemsFilledSoFar = 0;	// How many items we've filled in the current index.
		boolean newTruck = true;		// Flag that indicates if we need another truck.
		Truck currentTruck = null;	// The truck we are currently adding to.
		boolean finished = false;	// Flag which indicates when all the order is fulfilled.
		
		while (!finished) {
			Item currentItem = sortedList.get(currentIndex);
			if (newTruck) {
				newTruck = false;	// Acknowledge the flag.
				// What type of truck do we need?
				if (currentItem.getTemperature() == null) {
					// Since the list is sorted, we are guaranteed that there are no
					// more cold items after this point, so we are safe to use an ordinary truck.
					currentTruck = new OrdinaryTruck();
				} else {
					// There are cold items, so we must use a refrigerated truck. We can use the
					// current item's temperature because we are guaranteed that any subsequent items
					// are at that temperature or greater.
					currentTruck = new RefrigeratedTruck(currentItem.getTemperature());
				}
			}
			// How many items can we fill this iteration?
			int itemsToFill = Math.min(
					currentTruck.getCapacityRemaining(), // We can't fill more than the truck has left!
					order.getQuantity(currentItem) - itemsFilledSoFar // We can't fill more than we need!
			);

			currentTruck.addItem(currentItem, itemsToFill);
			// Add to our items counter.
			itemsFilledSoFar += itemsToFill;
			// Check if we're done with this item (we've filled the order quantity).
			if (itemsFilledSoFar == order.getQuantity(currentItem)) {
				// If we are, proceed to the next index (item) in sortedList.
				currentIndex++;
				itemsFilledSoFar = 0;
				// Did we just finish the last item?
				if (currentIndex == sortedList.size()) {
					finished = true;
				}
			}
			// Check if we need another truck (no capacity left in the current truck).
			// (Or, if we're finished, add the last truck anyway.)
			if (currentTruck.getCapacityRemaining() == 0 || finished) {
				// If we do, add the current truck to the manifest, and set the flag for a new one.
				newTruck = true;
				addTruck(currentTruck);
			}
		}
	}

	/**
	 * Returns the cost of the manifest by summing the cost of each truck within it.
	 * @return Returns the cost of the manifest as a double.
	 */
	public double getCost() {
		double cost = 0;
		for (Truck truck : trucks) {
			cost += truck.getCost();
		}
		return cost;
	}

	/**
	 * Adds a truck to the manifest.
	 * @param truck Truck to be added to manifest.
	 */
	public void addTruck(Truck truck) {
		trucks.add(truck);
	}
	
	/**
	 * Collates information about each truck and their stock within the manifest in the format required for CSV export.
	 * @param file Takes in the file to be written to.
	 */
	public void export(CSVWriter file) {
		for (Truck truck : trucks) {
			truck.export(file);
		}
	}
}
