/**
 * Tests for the Manifest class
 * @author Nathan Morrison
 */
package cab302_supermart;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Before;
import org.junit.Test;

import java.io.File;

public class ManifestTest {
	
	private static Random random = new Random();
	Manifest fest;

	@Before
	public void setUp() throws Exception {
		fest = null;
	}

	// Test Manifest has an empty constructor.
	@Test
	public void ManifestConstructorTest() {
		fest = new Manifest();
	}
	
	// Test the addTruck() method.
	@Test
	public void ManifestAddTruck() throws DeliveryException, StockException, CSVFormatException {
		fest = new Manifest();
		List<Truck> listOfTrucks = new ArrayList<Truck>();
		
		Stock stock1 = new Stock();
		Stock stock2 = new Stock();
		// populate the stocks
		Item parfait = new Item(
			"Perfect Parfait",
			10,
			5,
			10,
			40,
			null
		);
		Item ice_cream = new Item("ice cream",8,14,175,250,-20);
		stock1.addItem(parfait, 2);
		stock1.addItem(ice_cream, 4);
		stock2.addItem(ice_cream, 2);
		stock2.addItem(parfait, 4);
		
		int numberOfTrucks = 10;
		for (int i = 0; i < numberOfTrucks; i++) {
			boolean refrigeratedTruckType = ThreadLocalRandom.current().nextBoolean();
			Truck aTruck;
			
			if (refrigeratedTruckType) {
				aTruck = new RefrigeratedTruck(-10);
			}
			
			else {
				aTruck = new OrdinaryTruck();
			}
			
			fest.addTruck(aTruck);
			listOfTrucks.add(aTruck);
		}
		Manifest expectedManifest = new Manifest();
		for (Truck truck : listOfTrucks) {
			expectedManifest.addTruck(truck);
		}
		
		File file1 = new File("manifestTest.csv");
		File file2 = new File("manifest2Test.csv");
		
		CSVWriter csv1 = new CSVWriter(file1);
		CSVWriter csv2 = new CSVWriter(file2);
		
		fest.export(csv1);
		expectedManifest.export(csv2);
		
		csv1.close();
		csv2.close();
		
		CSVReader read1 = new CSVReader(file1);
		CSVReader read2 = new CSVReader(file2);
		
		String stringRead1 = "";
		String stringRead2 = "";
		
		for (ArrayList<String> line : read1) {
			stringRead1 += line;
		}
		
		for (ArrayList<String> line : read2) {
			stringRead2 += line;
		}
		
		assertEquals(stringRead1, stringRead2);
		
	}
	
	// Test the getCost() method.
	@Test
	public void ManifestGetCostTest() throws DeliveryException, StockException {
		// make some stocks
		Stock stock1 = new Stock();
		Stock stock2 = new Stock();
		// populate the stocks
		Item parfait = new Item(
			"Perfect Parfait",
			10,
			5,
			10,
			40,
			null
		);
		Item ice_cream = new Item("ice cream",8,14,175,250,-20);
		stock1.addItem(parfait, 2);
		stock1.addItem(ice_cream, 4);
		stock2.addItem(ice_cream, 2);
		stock2.addItem(parfait, 4);
		
		Truck truck1 = new RefrigeratedTruck(-20);
		Truck truck2 = new OrdinaryTruck();
		
		truck1.addItem(ice_cream, 6);
		truck2.addItem(parfait, 6);
		
		fest = new Manifest();
		fest.addTruck(truck1);
		fest.addTruck(truck2);
		double expectedCost = truck1.getCost() + truck2.getCost();
		assertEquals(expectedCost, fest.getCost(),0);
	}
	
	// Test the Fulfill method correctly splits up items, and adds non-temperature
	// sensitive items to refridgerated trucks when it's cheaper.
	// Manifest doesn't expose its internal state other than in getCost(), so we use that.
	@Test
	public void ManifestTestFulfill() throws StockException, DeliveryException {
		fest = new Manifest();
		Stock order = new Stock();
		Item macaron = new Item("Honey Butter Macaron", 5, 10, 40, 1000, -20);
		Item cup = new Item("Peanut Butter Cup", 2, 3, 80, 50, null);
		// We expect 800 macarons in one fridge truck, and an additional 200 in another.
		// (we can't fit all the macarons in one truck, in this test.)
		// The second truck should additionally contain 50 peanut butter cups (the
		// cups, even though they aren't temperature-sensitive, are in the cold truck)
		order.addItem(macaron, 1000);
		order.addItem(cup, 50);
		fest.fulfill(order);
		// Check the cost of the manifest is expected, two fridge trucks at -20 C.
		assertEquals(
			fest.getCost(),
			2*(900+(200*Math.pow(0.7, -20/5))) + macaron.getCost()*1000 + cup.getCost()*50,
			0
		);
	}
	
	// Test the Fulfill method correctly handles items that need to be in multiple trucks.
	@Test
	public void ManifestTestWithLotsofItems() throws StockException, DeliveryException {
		fest = new Manifest();
		Stock order = new Stock();
		Item riz = new Item("Riz au lait", 5, 10, 40, 50000, null);
		order.addItem(riz, 50000);
		fest.fulfill(order);
		// An ordinary Truck holds 1000 items, so we need 50 trucks to hold all of that
		// rice pudding.
		assertEquals(
			fest.getCost(),
			(750+0.25*1000)*50 + riz.getCost()*50000,
			0
		);
	}

	
}
