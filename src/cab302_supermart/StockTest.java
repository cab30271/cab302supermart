/**
 * Tests for the Stock class
 * @author Joshua Cao
 */

package cab302_supermart;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Before;
import org.junit.Test;

public class StockTest {
	
	Stock stock;

	
	//Clear the Item before each test.
	@Before
	public void setUp() throws Exception {
		stock = null;
	}
	
	public static int randInt(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}
	
	
	public static double randDouble(double min, double max) {
		return ThreadLocalRandom.current().nextDouble(min, max);
	}

	
	// Test that the Stock has the correct constructor.
	@Test
	public void StockHasCorrectConstructorTest() {
		// Stock has an empty constructor.
		stock = new Stock();
		assertEquals(0, stock.getCost(), 0);
	}
	
	
	// Test that the addItem method of the Stock correctly
	// updates the overall cost of the Stock.
	@Test
	public void StockAddItemTest() throws StockException {
		stock = new Stock();
		double cost = randDouble(5, 50);
		int item_count = randInt(1, 20);
		Item item = new Item(
			"Perfect Parfait",
			cost,
			5,
			10,
			item_count,
			null
		);
		stock.addItem(item, item_count);
		assertEquals(cost*item_count, stock.getCost(), 0);
	}
	
	
	
	// Test that getCost works correctly with multiple addItem calls.
	@Test
	public void StockGetCostTest() throws StockException {
		stock = new Stock();
		double[] costs = new double[] {4.23, 1.52, 6.43, 0.06, 6.66, 2.42};
		int[] quantities = new int[] {4, 2, 6, 1, 8, 22};
		double expectedCost = 0;
		for (int i = 0; i < costs.length; i++) {
			expectedCost += (costs[i]*quantities[i]);
			// make a mock item
			Item item = new Item(
				Integer.toString(i),
				costs[i],
				5,
				10,
				60,
				-10
			);
			stock.addItem(item, quantities[i]);
		}
		assertEquals(expectedCost, stock.getCost(), 0);
	}
	
	// Test the getItems method.
	@Test
	public void StockGetItemsTest() throws StockException {
		stock = new Stock();
		Item item = new Item(
				"Matcha Bavarian with Cream",
				5,
				5,
				10,
				60,
				-10
		);
		HashMap<Item, Integer> expectedValue = new HashMap<Item, Integer>();
		expectedValue.put(item, 5);
		stock.addItem(item, 5);
		assertEquals(stock.getItems(), expectedValue);
	}
	
	// Test the getQuantity method.
	@Test
	public void StockGetQuantityTest() throws StockException {
		stock = new Stock();
		Item item = new Item(
			"Matcha Bavarian with Cream",
			5,
			5,
			10,
			60,
			-10
		);
		int expectedQuantity = 5;
		stock.addItem(item, expectedQuantity);
		assertEquals(stock.getQuantity(item), expectedQuantity);
	}
	
	// Test the getQuantity method with an Item of the same values.
	@Test
	public void StockGetQuantityTestSameValueDifferentReference() throws StockException {
		stock = new Stock();
		Item item = new Item(
			"Matcha Bavarian with Cream",
			5,
			5,
			10,
			60,
			-10
		);
		Item item2 = new Item(
			"Matcha Bavarian with Cream",
			5,
			5,
			10,
			60,
			-10
		);
		int expectedQuantity = 5;
		stock.addItem(item, expectedQuantity);
		assertEquals(stock.getQuantity(item2), expectedQuantity);
	}
	
	// Test that the addItem method updates existing values relatively (i.e. it adds to them)
	@Test
	public void StockAddItemRelativelyUpdatesExistingKeysTest() throws StockException {
		stock = new Stock();
		Item item = new Item(
			"Matcha Bavarian with Cream",
			5,
			5,
			10,
			60,
			-10
		);
		int expectedQuantity = 8;
		stock.addItem(item, 5);
		stock.addItem(item, 3);
		assertEquals(stock.getQuantity(item), expectedQuantity);
	}
	
	// Test that the addItem method can also subtract from existing values.
	@Test
	public void StockAddItemSubtractsExistingKeys() throws StockException {
		stock = new Stock();
		Item item = new Item(
			"Matcha Bavarian with Cream",
			5,
			5,
			10,
			60,
			-10
		);
		int expectedQuantity = 8;
		stock.addItem(item, 11);
		stock.addItem(item, -3);
		assertEquals(stock.getQuantity(item), expectedQuantity);
	}
	
	// Test that the addItem method throws StockException if it would subtract below zero.
	@Test (expected = StockException.class)
	public void StockAddItemThrowsStockExceptionIfBelowZero() throws StockException {
		stock = new Stock();
		Item item = new Item(
			"Matcha Bavarian with Cream",
			5,
			5,
			10,
			60,
			-10
		);
		int expectedQuantity = 1;
		stock.addItem(item, 1);
		stock.addItem(item, -3);	// should not affect the quantity.
		assertEquals(stock.getQuantity(item), expectedQuantity);
	}
}
