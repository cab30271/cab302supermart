/**
 * CSV Writing library, etc, etc
 * @author Joshua Cao
 */

package cab302_supermart;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class CSVWriter {
	
	private BufferedWriter writer;

	public CSVWriter(File file) throws CSVFormatException {
		// TODO Auto-generated constructor stub
		try {
			writer = new BufferedWriter(new FileWriter(file));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// not sure what IOException triggers on???
			throw new CSVFormatException("File access denied. (Is it read-only?)");
		}
	}
	
	
	/**
	 * Writes an array as one line to the CSV file, separated by commas
	 * @param input the array to write
	 */
	public void writeLine(ArrayList<String> input) {
		// variable for not doing something on the first iteration.
		boolean isFirst = true; 
		try {
			for (String s : input) {
				// Don't write the comma in the first iteration.
				if (!isFirst) {
					writer.write(",");
				}
				isFirst = false;
				writer.write(s);
			} 
			writer.write("\n");
		} catch (IOException e) {
			// do something here I guess??
		}
	}
	
	
	/**
	 * Overload for writeLine if you just want to write a single string.
	 * @param input the string to write.
	 */
	public void writeLine(String input) {
		try {
			writer.write(input);
			writer.write("\n");
		} catch (IOException e) {
			System.out.println("writeLine");
		}
	}
	
	/**
	 * Closes the CSVWriter.
	 */
	public void close() {
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
