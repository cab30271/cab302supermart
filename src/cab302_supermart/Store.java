/**
 * Class that represents a SuperMart Store.
 * @author Joshua Cao & Nathan Morrison
 */
package cab302_supermart;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class Store {
	
	private Stock inventory = null;
	private String storeName;
	private double capital;
	private HashMap<String, Item> stringItemMap;
	private final int MINIMUM_TEMPERATURE = -20;
	
	/**
	 * Initialise the store.
	 * @param storeName. The name of the store.
	 * @param capital. The starting capital of the store.
	 * @throws StockException 
	 */
	public Store(String storeName, double capital) throws StockException {
		this.storeName = storeName;
		this.capital = capital;
		this.inventory = new Stock();
		this.stringItemMap = new HashMap<String, Item>();
	}
	
	/**
	 * Initialise the Items that the store sells from an item properties file.
	 * Note: resets quantities to 0. 
	 * @param file - The File object to load the item properties from.
	 * @throws CSVFormatException 
	 * @throws StockException 
	 */
	public void loadItemProperties(File file) throws CSVFormatException, StockException {
		CSVReader reader = new CSVReader(file);
		boolean enteredOnce = false;
		for (ArrayList<String> arr : reader) {
			enteredOnce = true;
			// Check if we have the correct number of items.
			if (arr.size() != 5 && arr.size() != 6) {
				throw new CSVFormatException("Incorrect number of items in file!");
			}
			Item newItem;
			try {
				newItem = new Item(
					arr.get(0),		// item name
					Double.parseDouble(arr.get(1)),						// item cost
					Double.parseDouble(arr.get(2)),						// item price
					Integer.parseInt(arr.get(3)),						// reorder point
					Integer.parseInt(arr.get(4)),						// reorder amount
					arr.size() > 5 ? Integer.parseInt(arr.get(5)) : null	// temperature
				);
			} catch (NumberFormatException e) {
				throw new CSVFormatException("CSV numeric parsing error.");
			}
			inventory.addItem(newItem, 0);
			stringItemMap.put(arr.get(0), newItem);
		}
		if (!enteredOnce) {
			throw new CSVFormatException("Does not seem to be an item properties file!");
		}
	}
	
	/**
	 * Export a Manifest to the given File.
	 * @param file - The file to write the Manifest to.
	 * @throws DeliveryException - Thrown if the Manifest we generated is illegal.
	 * @throws StockException 
	 * @throws CSVFormatException 
	 */
	public void exportManifest(File file) throws DeliveryException, StockException, CSVFormatException {
		// First, make a Stock which is what we are going to order
		Stock order = new Stock();
		for (Item it : inventory.getItems().keySet()) {
			// Are we less than the reorder point?
			if (inventory.getQuantity(it) < it.getReorderPoint()) {
				// If we are, add that item to the order.
				order.addItem(it, it.getReorderAmount());
			}
		}
		// special case: we need nothing
		if (order.getItems().size() == 0) {
			throw new StockException("You don't need anything more!");
		}
		
		// Make the Manifest to fulfill the order
		Manifest manifest = new Manifest();
		manifest.fulfill(order);

		CSVWriter writer = new CSVWriter(file);
		manifest.export(writer);
		writer.close();
	}

	/**
	 * Load a manifest into the store. It will take a CSV file of trucks and their stock.
	 * This will cause the stock in the store to increase, whilst decreasing the capital of the store.
	 * @param file File to be loaded into the store.
	 * @throws DeliveryException
	 * @throws StockException
	 * @throws CSVFormatException
	 */
	public void loadManifest(File file) throws DeliveryException, StockException, CSVFormatException {
		//Prepare to read file into system with required variables
		CSVReader manifestReading = new CSVReader(file);
		Manifest manifest = new Manifest();
		Truck currentTruck = null;
		int truckTemperature = 10;
		
		//Read CSV file line by line.
		for (ArrayList<String> line : manifestReading) {
			
			//If there is only one column on a line, it indicates that it's
			//the start of a new truck.
			if (line.size() == 1) {
				
				//Add the truck to the manifest that we've already been adding items to.
				//First make sure to update the temperature of the truck to required value
				//if it is a RefrigeratedTruck
				if (currentTruck != null) {
					if (currentTruck.getName() == "Refrigerated") {
						
						((RefrigeratedTruck)currentTruck).updateTemperature(truckTemperature);
					}
					manifest.addTruck(currentTruck);
				}
				
				//Create a RefrigeratedTruck if the CSV file says to.
				//Set its temperature to the minimum allowable so that any
				//items can be added to it. But keep track of what the minimum
				//temperature required for the truck is with truckTemperature.
				if ((line.get(0).toString()).equals(">Refrigerated")) {

					truckTemperature = 10;
					currentTruck = new RefrigeratedTruck(MINIMUM_TEMPERATURE);
				}
				
				//Create an OrdinaryTruck if the CSV file says to.
				else if (line.get(0).toString().equals(">Ordinary")) {
					currentTruck = new OrdinaryTruck();	
				}
				
				//If the CSV file says anything else, then this is an invalid manifest file.
				else {
					throw new CSVFormatException(
							"Error loading manifest. Invalid line in file. Can't read: " 
							+ line.get(0).toString());
				}
			}
			
			//If there are two columns on a line. This is an item and its quantity.
			else if (line.size() == 2) {
				Item item;
				//Find the item in the inventory
				item = this.getItemByName(line.get(0));
				//If the item cannot be found, the item's properties have not been loaded
				//Therefore it is an invalid item.
				if (item == null) {
					throw new CSVFormatException("Unrecognised item. Tried to add: \t"
												 + line.get(0).toString());
				}
				
				//Check if items belong to a truck.
				if (currentTruck == null) {
					throw new CSVFormatException("Found items without a Truck.");
				}
				
				//Update truckTemperature if required.
				if (item.getTemperature() == null) {
					//Do nothing
				}
				else if (item.getTemperature() < truckTemperature && currentTruck.getName() == "Refrigerated") {
					truckTemperature = item.getTemperature();
				}
				
				
				Integer quantity = Integer.parseInt(line.get(1));
				//As this is a manifest, the quantity of items in the inventory will increase.
				currentTruck.addItem(item, quantity);
				inventory.addItem(item, quantity);
			}
			//If there are more than two columns in the CSV it is an invalid manifest.
			else {throw new CSVFormatException("Error loading manifest. Too many columns in:"
												+ line.toString());}
		}
		
		//If we get to this point without having created a truck, then the file must be empty.
		if (currentTruck == null) {
			throw new CSVFormatException("That's an empty file!");
		}
		
		//Make sure to add the last truck in the manifest.
		manifest.addTruck(currentTruck);
		//Update the capital by paying for the manifest.
		this.capital -= manifest.getCost();
	}
	
	/**
	 * Loads a sales log in a CSV file. This will cause the capital to increase and the
	 * quantity of the items in the store to decrease.
	 * @param file CSV file that is to be read.
	 * @throws StockException
	 * @throws CSVFormatException
	 */
	public void loadSalesLog(File file) throws StockException, CSVFormatException {
		//Prepare file for reading.
		CSVReader salesLogReading = new CSVReader(file);
		boolean enteredOnce = false;
		
		//Read through the file line by line.
		for (ArrayList<String> line : salesLogReading) {
			enteredOnce = true;
			
			//CSV file must consist of two columns, item name followed by quantity of items sold.
			if (line.size() != 2) {
				throw new CSVFormatException("Unexpected number of columns");
			}
			Item item = getItemByName(line.get(0));
			//If the item cannot be found, the item's properties have not been loaded
			//Therefore it is an invalid item.
			if (item == null) {
				throw new CSVFormatException("Unrecognised item. Tried to sell: \t"
											 + line.get(0).toString());
			}
			//How many items were sold?
			Integer quantity = Integer.parseInt(line.get(1));
			//As this is a sales log, decrease the quantity of items in the inventory.
			inventory.addItem(item, -quantity);
			//Increase the capital by the required amount.
			capital += item.getPrice() * quantity;
		}
		
		//If we get to here without reading a line then the file is empty.
		if (!enteredOnce) {
			throw new CSVFormatException("That file is empty!");
		}
	}
	
	/**
	 * Used to return the Item type of an item when provided with the item name as a String.
	 * @param itemName This is a String representation of the item.
	 * @return returns the Item object.
	 */
	public Item getItemByName(String itemName) {	
		return stringItemMap.get(itemName);
	}
	/**
	 * Return the inventory of the Store.
	 * @return A Stock object which is the inventory of the store.
	 */
	public Stock getStock() {
		return inventory;
	}
	
	/**
	 * Returns how much capital the store has.
	 * @return Returns a double that represents the capital of the store.
	 */
	public double getCapital() {
		return capital;
	}
	
	/**
	 * @return Returns the name of the store.
	 */
	public String getName() {
		return storeName;
	}

	/**
	 * Used to change the name of a store. Useful for rebranding.
	 * @param storeName
	 * @throws NameException
	 */
	public void changeName(String storeName) throws NameException {
		// Do nothing if the cancel button was clicked.
		if (storeName == null) {
			return;
		}
		
		if (storeName.length() == 0) {
			throw new NameException("You cannot have a 0-character name. Store name is now Default Store.");
		}
		this.storeName = storeName;
	}
	
	/**
	 * Used to reset the store stock if an error occurs while attempting to load item properties.
	 */
	public void resetStoreStock() {
		inventory = new Stock();
		stringItemMap = new HashMap<String, Item>(); 
	}
}